<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Inbound_fa fa-caret-down</name>
   <tag></tag>
   <elementGuidId>b651f7d0-ed0d-4b0f-9fb7-15d690deab8d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div/a/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>i.fa.fa-caret-down</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-class</name>
      <type>Main</type>
      <value>{'fa fa-caret-down':!isClicked[$index], 'fa fa-caret-up':isClicked[$index]}</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-caret-down</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;ng-scope&quot;]/body[@class=&quot;ng-scope block-ui block-ui-anim-fade&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;wms-header ng-scope&quot;]/div[@class=&quot;container-fluid no-padding&quot;]/div[@class=&quot;masthead ng-scope&quot;]/nav[1]/ul[@class=&quot;nav nav-justified&quot;]/li[@class=&quot;ng-scope&quot;]/div[@class=&quot;dropdown ng-scope&quot;]/div[@class=&quot;data-list&quot;]/a[@class=&quot;dropdown-toggle disabled ng-binding&quot;]/i[@class=&quot;fa fa-caret-down&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/a/i</value>
   </webElementXpaths>
</WebElementEntity>
