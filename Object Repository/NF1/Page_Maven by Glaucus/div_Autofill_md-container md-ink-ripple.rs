<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Autofill_md-container md-ink-ripple</name>
   <tag></tag>
   <elementGuidId>077ec1dc-69cd-4b5e-8e72-a5a215296bd6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//md-checkbox[@id='POInvCheckbox2']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#POInvCheckbox2 > div.md-container.md-ink-ripple</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>md-container md-ink-ripple</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;POInvCheckbox2&quot;)/div[@class=&quot;md-container md-ink-ripple&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//md-checkbox[@id='POInvCheckbox2']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/span/md-checkbox/div</value>
   </webElementXpaths>
</WebElementEntity>
