<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Close                                  _9a252d</name>
   <tag></tag>
   <elementGuidId>ea6ad7c8-1592-43eb-bc52-0e8bdfb3602e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='editUnload']/div/form/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>form[name=&quot;editUnloadForm&quot;] > div.modal-content</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-content</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                    
                        ×
                        Close
                    
                    Unloading
                    
                
                
                    

                        
                            
                                Transaction Type
                                    *
                                
                            
                            
                                
                                        Select Transaction Type
                                    Purchase orderStock TransferSales ReturnDirect InwardDirect Sale ReturnReturnable Goods Order
                                Transaction Type is Required
                                

                                
                                    Warning: Cannot be changed later.
                                    
                            
                        


                        
                        
                            
                                
                                    Select Vendor 
                                    *
                                

                                

                                
                                    

                                                Searching...    No results found      
                                    
                                    

                                    Vendor is Required
                                        

                                    
                                            Warning: Cannot be changed later.
                                        
                                

                                
                                    
                                        . . .
                                    
                                
                            
                            
                                
                                    PO
                                    *
                                

                                

                                

                                    
                                        
                                                Searching...    No results found      
                                    
                                    
                                    PO is Required
                                    

                                    
                                        Warning: Cannot be changed later.
                                        
                                    
                                
                                
                                    
                                        . . .
                                    
                                
                            
                        

                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        

                        
                        
                            Reference Details
                        

                        
                            
                                
                                    External Ref No
                                
                                

                                    
                                        
                                    
                                
                            
                        
                        
                            
                                
                                    
                                        LR Number *
                                    
                                    
                                        

                                    
                                    
                                    
                                        
                                                
                                                
                                                
                                                Add
                                                LR Image
                                            
                                        
                                        
                                    
                                    
                                        No Image Selected
                                        
                                    
                                    
                                
                            
                            
                                
                                    
                                        
                                            
                                            
                                                Invoice Number
                                                *
                                            

                                        
                                
                                    
                                        
                                   
                                        
                                            
                                        
                                    
                                    Enter Invoice Number
                                    
                                        
                                            
                                            
                                            Change
                                            

                                            
                                            
                                                 Invoice Image
                                                *
                                                
                                            

                                        
                                        
                                        
                                        
                                    
                                    
                                    
                                        
                                        
                                            
                                                
                                                    
                                                
                                        
                                    
                                    
                                
                            
                            
                                
                                    
                                        
                                            EWB Number
                                        
                                    
                                    
                                        
                                    
                                    
                                    
                                        
                                                
                                                
                                                
                                                Add
                                                EWB Image
                                            
                                        
                                        
                                        
                                    
                                        No Image Selected
                                        
                                    
                                    
                                
                            
                        

                        
                        

                        

                        
                            Add Unloading Location And
                                Pallets
                        
                        
                            
                                
                                Location
                                    *
                                
                                
                                
                                    
                                
                                
                                    
                                        . . .
                                    
                                
                                
                                
                                    Pallet
                                
                                
                                    
                                
                                
                                    
                                        . . .
                                    
                                
                                
                                    
                                

                                

                                
                                    
                                        
                                        
                                            
                                                Location
                                            
                                            
                                                Pallet
                                            
                                            
                                                Action
                                            
                                        
                                        
                                        
                                        
                                            
                                                Default_Unloading
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                            
                                        
                                    
                                

                            
                        


                        
                        
                            Box Details
                        

                        
                            
                                
                                    
                                    
                                        
                                            Box Barcode
                                        
                                        
                                            Box Count
                                        
                                        
                                            Box Actual Weight
                                        
                                        
                                            Box Expected Weight
                                        
                                        
                                            Weight Unit
                                        
                                        
                                            Action
                                        
                                    
                                    
                                    
                                    
                                        
                                            
                                        
                                        
                                            
                                        
                                        
                                            
                                        
                                        
                                            
                                        
                                        
                                            
                                                    Select Weight Unit
                                                KgGramPound
                                        
                                        
                                            
                                            
                                        
                                    
                                
                            
                        
                        
                        
                        
                            
                                

                                    
                                        
                                            
                                            
                                        
                                        
                                    
                                
                            
                        
                        
                             
                            
                        
                        

                    
                
                
                    
                        Cancel
                    
                    
                        Save
                        
                    


                
            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;editUnload&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/form[@class=&quot;ng-invalid ng-invalid-required ng-dirty ng-valid-parse ng-valid-maxlength&quot;]/div[@class=&quot;modal-content&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='editUnload']/div/form/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Yes'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//action-unloading/div/div/form/div</value>
   </webElementXpaths>
</WebElementEntity>
