<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_BOXMW00016994_fa fa-plus fa-lg</name>
   <tag></tag>
   <elementGuidId>441b18cf-6263-4b2e-b9b0-509d0aeee788</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//span[@id='Select Bin']/span)[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-plus fa-lg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;boxListPopUp&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/form[@class=&quot;ng-pristine ng-valid&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;col-lg-12 col-md-12 col-sm-12 col-xs-12&quot;]/div[1]/table[@class=&quot;table table-no-margin&quot;]/tbody[1]/tr[@class=&quot;no-padding ng-scope&quot;]/td[@class=&quot;text-center&quot;]/span[@id=&quot;Select Bin&quot;]/span[@class=&quot;fa fa-plus fa-lg&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//span[@id='Select Bin']/span)[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/span/span</value>
   </webElementXpaths>
</WebElementEntity>
