<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Box No.  BOXMW00016994                 _b7fe99</name>
   <tag></tag>
   <elementGuidId>409bb0f1-11d2-40b5-b17a-70d98934029a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='editQC']/div/div/div[2]/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-md-8.ShipProduct.height_400</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-8 ShipProduct height_400</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-class</name>
      <type>Main</type>
      <value>(genericData.parentQCCount.inRackQc)?'height_500':'height_400'</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                                
                                    Box No. : BOXMW00016994
                                
                                    Total Scanned Quantity : 10
                                
                                
                                    Bin No. : BINMW00016995
                                
                                
                                    
                                        
                                        

                                            
                                                SKU Code
                                            
                                            
                                                SKU Name
                                            
                                            
                                                Scanned Qty.(Eaches)
                                            
                                            
                                                Number of Pack
                                            
                                            
                                            
                                                Total Count
                                            
                                            
                                                Expected Qty
                                            
                                        
                                        
                                        
                                        
                                        
                                            
                                                Kite 01
                                            
                                            
                                                Kite 01
                                            
                                            
                                                
                                                    10
                                                
                                            
                                            
                                                
                                                
                                                
                                            
                                            
                                                
                                            
                                            
                                            -
                                            -
                                        
                                        
                                    
                                
                            
                        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;editQC&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-8 ShipProduct height_400&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='editQC']/div/div/div[2]/div/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SampleQC'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='USN'])[1]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//action-q-c/div/div/div/div[2]/div/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
