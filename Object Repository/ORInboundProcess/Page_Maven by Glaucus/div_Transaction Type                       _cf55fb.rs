<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Transaction Type                       _cf55fb</name>
   <tag></tag>
   <elementGuidId>7ed8b4a0-e4ac-4f3c-af96-3ccb1c5084c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='actionUnload']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#actionUnload</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-body</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>actionUnload</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    

                        
                            
                                Transaction Type
                                    *
                                
                            
                            
                                
                                        Select Transaction Type
                                    Purchase orderStock TransferSales ReturnDirect InwardDirect Sale ReturnReturnable Goods Order
                                Transaction Type is Required
                                

                                
                                    Warning: Cannot be changed later.
                                    
                            
                        


                        
                        
                            
                                
                                    Select Vendor 
                                    *
                                

                                

                                
                                    

                                                Searching...    No results found      
                                    
                                    

                                    Vendor is Required
                                        

                                    
                                            Warning: Cannot be changed later.
                                        
                                

                                
                                    
                                        . . .
                                    
                                
                            
                            
                                
                                    PO
                                    *
                                

                                

                                

                                    
                                        
                                                Searching...    No results found      
                                    
                                    
                                    PO is Required
                                    

                                    
                                        Warning: Cannot be changed later.
                                        
                                    
                                
                                
                                    
                                        . . .
                                    
                                
                            
                        

                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        

                        
                        
                            Reference Details
                        

                        
                            
                                
                                    External Ref No
                                
                                

                                    
                                        
                                    
                                
                            
                        
                        
                            
                                
                                    
                                        LR Number *
                                    
                                    
                                        

                                    
                                    
                                    
                                        
                                                
                                                
                                                
                                                Add
                                                LR Image
                                            
                                        
                                        
                                    
                                    
                                        No Image Selected
                                        
                                    
                                    
                                
                            
                            
                                
                                    
                                        
                                            
                                            
                                                Invoice Number
                                                *
                                            

                                        
                                
                                    
                                        
                                   
                                        
                                            
                                        
                                    
                                    Enter Invoice Number
                                    
                                        
                                            
                                            
                                            Change
                                            

                                            
                                            
                                                 Invoice Image
                                                *
                                                
                                            

                                        
                                        
                                        
                                        
                                    
                                    
                                    
                                        
                                        
                                            
                                                
                                                    
                                                
                                        
                                    
                                    
                                
                            
                            
                                
                                    
                                        
                                            EWB Number
                                        
                                    
                                    
                                        
                                    
                                    
                                    
                                        
                                                
                                                
                                                
                                                Add
                                                EWB Image
                                            
                                        
                                        
                                        
                                    
                                        No Image Selected
                                        
                                    
                                    
                                
                            
                        

                        
                        

                        

                        
                            Add Unloading Location And
                                Pallets
                        
                        
                            
                                
                                Location
                                    *
                                
                                
                                
                                    
                                
                                
                                    
                                        . . .
                                    
                                
                                
                                
                                    Pallet
                                
                                
                                    
                                
                                
                                    
                                        . . .
                                    
                                
                                
                                    
                                

                                

                                
                                    
                                        
                                        
                                            
                                                Location
                                            
                                            
                                                Pallet
                                            
                                            
                                                Action
                                            
                                        
                                        
                                        
                                        
                                            
                                                Default_Unloading
                                            
                                            
                                                
                                                
                                            
                                            
                                                
                                            
                                        
                                    
                                

                            
                        


                        
                        
                            Box Details
                        

                        
                            
                                
                                    
                                    
                                        
                                            Box Barcode
                                        
                                        
                                            Box Count
                                        
                                        
                                            Box Actual Weight
                                        
                                        
                                            Box Expected Weight
                                        
                                        
                                            Weight Unit
                                        
                                        
                                            Action
                                        
                                    
                                    
                                    
                                    
                                        
                                            
                                        
                                        
                                            
                                        
                                        
                                            
                                        
                                        
                                            
                                        
                                        
                                            
                                                    Select Weight Unit
                                                KgGramPound
                                        
                                        
                                            
                                            
                                        
                                    
                                
                            
                        
                        
                        
                        
                            
                                

                                    
                                        
                                            
                                            
                                        
                                        
                                    
                                
                            
                        
                        
                             
                            
                        
                        

                    
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;actionUnload&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='actionUnload']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='editUnload']/div/form/div/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[10]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Yes'])[1]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//action-unloading/div/div/form/div/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
