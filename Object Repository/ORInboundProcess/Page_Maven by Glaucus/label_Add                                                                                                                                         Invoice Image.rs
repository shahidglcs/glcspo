<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Add                                                                                                                                         Invoice Image</name>
   <tag></tag>
   <elementGuidId>f3b959b3-7aeb-4add-a1d2-3494747553ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[3]/following::label[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '
                                            
                                            
                                            
                                            Add

                                            
                                            
                                                 Invoice Image
                                                *
                                                
                                            

                                        ' or . = '
                                            
                                            
                                            
                                            Add

                                            
                                            
                                                 Invoice Image
                                                *
                                                
                                            

                                        ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>unloading-custom-file-upload</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            
                                            
                                            
                                            Add

                                            
                                            
                                                 Invoice Image
                                                *
                                                
                                            

                                        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;actionUnload&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;col-sm-12 col-md-12 no-padding&quot;]/div[@class=&quot;col-md-4&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/label[@class=&quot;unloading-custom-file-upload&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='actionUnload']/div/div[4]/div[2]/div/div[4]/label</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[3]/following::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/label</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//input[@name='InvoiceNumber']</value>
   </webElementXpaths>
</WebElementEntity>
