<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_In Process                             _b8d0af</name>
   <tag></tag>
   <elementGuidId>ef9d57cb-b21e-4d83-94f5-c356b8b845e4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Putaway'])[2]/following::div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-init</name>
      <type>Main</type>
      <value>onPageInitialize()</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container-fluid</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    
        
            
                
                    
                        In Process
                    
                
                    
                        New
                    
                
                    
                        Completed
                    
                
                    
                        Cancelled
                    
                
            
        
        
            
                
                    Total Pending Quantity
                    
                
                
                    
                        
                        0
                        
                        
                    
                
            
            
                
                    Total Pending Orders
                    
                
                
                    
                        
                        2
                        
                        
                    
                
            
        
    

    

    
        
            
                
                    
                        
                            
                                
                                    Task ID
                                    Gatepass Number
                                    
                                    GRN Number
                                    Order Type
                                    Task Type
                                    Qty
                                    Inventory Grade
                                    Order Ref. No.
                                    
                                    Order Pending For (hr)
                                        
                                    
                                    Task Pending For (hr)
                                        
                                    
                                    
                                        Action
                                    
                                
                            
                            
                            
                                    
                                        14559
                                    
                                        
                                        
                                    
                                    
                                    
                                        GATEP0000001824
                                    
                                    
                                        GRN2101338
                                    
                                    
                                        
                                            PO
                                        
                                        
                                    
                                    
                                     PutAway   
                                    
                                    
                                        160
                                    
                                    
                                        Good
                                    
                                    
                                        
                                        
                                            258210000E7
                                        
                                        
                                        
                                    

                                    
                                        
                                            210
                                        
                                        

                                        
                                            -
                                        
                                    
                                    
                                        210
                                    
                                    
                                        
                                            
                                        
                                        
                                    
                                    
                                    
                                
                                 
                                        
                                        
                                         
                                    
                                
                            
                            
                                    
                                        10568
                                    
                                        
                                        
                                    
                                    
                                    
                                        GATEP0000001393
                                    
                                    
                                        GRN2100978
                                    
                                    
                                        
                                            PO
                                        
                                        
                                    
                                    
                                     PutAway   
                                    
                                    
                                        65
                                    
                                    
                                        Good
                                    
                                    
                                        
                                        
                                            2582100006F
                                        
                                        
                                        
                                    

                                    
                                        
                                            2857
                                        
                                        

                                        
                                            -
                                        
                                    
                                    
                                        2857
                                    
                                    
                                        
                                            
                                            
                                        
                                        
                                    
                                    
                                
                                 
                                        
                                        
                                         
                                    
                                
                            
                            

                            

                            
                        
                    
                
            
        
    
    
    
    
        
            
                
                    
                        ×
                        Close
                    
                    
                        Add 
                        Putaway Form
                    
                
                
                    
                        
                            
                                From Bin
                            
                            
                                
                                    
                                

                            
                        
                        
                            
                                To Bin
                            
                            
                                
                                    
                                

                            
                        
                    
                    

                        
                            
                                From Location
                            
                            
                                
                                    
                                

                            
                        
                        
                            
                                To Location
                            
                            
                                
                                    
                                

                            
                        
                    

                    
                        
                        
                            
                                
                            
                        
                        
                            
                                Proceed
                            
                            
                        
                    
                    
                        
                    

                    
                
            
            
                
                    Cancel
                
                
                    Start Putaway
                
            
        
    


        
        




        


    .invisible {
        visibility: hidden;
    }



    
        
            
                
                    
                        ×
                        Close
                    
                    Confirmation
                
                
                    
                        
                            
                                
                                
                                        
                                    ?
                                
                            
                        
                    
                
                
                    
                        No
                    
                    
                        Yes
                    
                
            
        
    


    
        
            
                
                    
                        ×
                        Close
                    
                    Empty Bin(s) List
                
                
                    
                        
                            
                                
                                    Following 
                                    are the list of empty bin(s) at this location.
                                    Select bin(s) which you would like to change its location to Default_QC
                                
                            
                            
                        
                    
                
                
                    
                        Cancel
                    
                    
                        Done
                    
                
            
        
    


    .inofmessage{
        font-size: 16px;
        font-weight: 600;
        line-height: 25px;
        margin: 10px 0 24px;
        text-align: center;
    }
    .inline-check{
        font-weight: 600;
    }
    .inline-check md-checkbox, .inline-check span {
        float: left;
        color: #8e8e8e;
    }
    .popcancel-btn.btn-first:hover {
        border-color: #727374;
    }


    .popcancel-btn.btn-first {
        background: #727374;
    }

    .modal-content, .modal-footer {
        border-radius: 0;
        border: none;
        box-shadow: none;
    }

    .modal-footer {
        background: #30373B;
        min-height: 60px;
    }

    .modal-header {
        background: #E8F7FC;
    }
    .modal-header .close, md-toolbar .close {
        opacity: 0.7;
        transition: opacity 0.3s ease-in-out;
    }

    .modal-header .close, md-toolbar .close {
        opacity: 0.7;
        transition: opacity 0.3s ease-in-out;
    }

    .modal-header h4, md-toolbar h4.head {
        font-size: 22px;
        font-weight: 600;
        margin-top: 0;
        margin-bottom: 0;
        color: #333;
    }


    
        
            
                
                    
                        ×
                        Close
                    
                    Confirmation
                
                
                    
                        
                            
                                This will reset the scanned data.Proceed?
                            
                        
                    
                
                
                    
                        No
                    
                    
                        Yes
                    
                
            
        
    

    

    
        
            
                
                    
                        ×
                        Close
                    
                    Pending Bin List
                
                
                    
                                            
                            
                                
                                
                                    
                                        Bin Code
                                    
                                    
                                        Bin Type
                                    
                                
                                
                                
                                
                                
                                                   
                    
                
                
                
                    Close
                    
                
            
        
    



</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;ng-scope&quot;]/body[@class=&quot;ng-scope block-ui block-ui-anim-fade&quot;]/ui-view[@class=&quot;ng-scope&quot;]/putaway-component[@class=&quot;ng-scope ng-isolate-scope&quot;]/div[@class=&quot;container-fluid&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Putaway'])[2]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[6]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//putaway-component/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
