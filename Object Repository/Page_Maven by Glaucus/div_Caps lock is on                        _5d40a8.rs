<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Caps lock is on                        _5d40a8</name>
   <tag></tag>
   <elementGuidId>28e0e46d-73ba-4c8e-ac6e-ce3329641db3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.flex-section</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Log In'])[1]/preceding::div[6]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>flex-section</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>



                    
                        
                            
                        
                        
                    



                    




                    
                         
                            

                        
                        
                        Caps lock is on
                    




                    
                        Forgot password ?
                    
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;ng-scope&quot;]/body[1]/div[@class=&quot;ng-scope&quot;]/section[@class=&quot;register-container ng-scope&quot;]/div[@class=&quot;form-wrapper-om small-width&quot;]/div[@class=&quot;form-design login style-one no-padding&quot;]/form[@class=&quot;main-form ng-valid ng-dirty ng-valid-parse&quot;]/div[@class=&quot;sign-up-input&quot;]/div[@class=&quot;flex-section&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log In'])[1]/preceding::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div</value>
   </webElementXpaths>
</WebElementEntity>
