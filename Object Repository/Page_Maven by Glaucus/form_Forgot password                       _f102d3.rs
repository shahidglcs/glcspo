<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>form_Forgot password                       _f102d3</name>
   <tag></tag>
   <elementGuidId>20aa1514-a3a9-41e9-8446-da2323cd1cbb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>form.main-form.ng-pristine.ng-valid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>main-form ng-pristine ng-valid</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                



                    
                        
                            
                        
                        
                    



                    




                    




                    
                        Forgot password ?
                    
                
            
            
                Next
                
            

            OR
            
                If you're not a registered user, please contact
             support@gscmaven.com
            


        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;ng-scope&quot;]/body[1]/div[@class=&quot;ng-scope&quot;]/section[@class=&quot;register-container ng-scope&quot;]/div[@class=&quot;form-wrapper-om small-width&quot;]/div[@class=&quot;form-design login style-one no-padding&quot;]/form[@class=&quot;main-form ng-pristine ng-valid&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form</value>
   </webElementXpaths>
</WebElementEntity>
